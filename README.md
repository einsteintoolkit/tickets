Welcome to the [Einstein Toolkit](https://www.einsteintoolkit.org) central issue tracker.

* [to the tickets](https://bitbucket.org/einsteintoolkit/tickets/issues)

Einstein Toolkit: ![Jenkins build badge](https://jenkins.einsteintoolkit.ncsa.illinois.edu/job/EinsteinToolkit/badge/icon)